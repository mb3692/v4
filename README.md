# Quick start

~~~~ {.bash}
~/workspace $ git clone <URL-to-repo>
~/workspace $ cd <repo>

~/workspace/v4 (master) $ npm install //set express app
~/workspace/v4 (master) $ npm start   //host app, or use nodemon - installed globally!
~~~~


## Heroku
Before running 

`heroku login -i`

we need to set up Heroku CLI for our workspace by running

~~~~ {.bash}
~/workspace $ curl https://cli-assets.heroku.com/install.sh | sh
~/workspace $ sudo ln -s /usr/local/bin/heroku /usr/bin/heroku
~/workspace $ cd <repo>
~~~~

After that, you can test if CLI is instaled properly by running

` heroku local`

which will serve your app locallz, just as with `npm start`.

## MongoDB

Be careful to first create and serve local database before accesing it!

## The trial database links

URI:
`mongodb://user:password1@ds115874.mlab.com:15874/comments`

in shell:
`mongo ds115874.mlab.com:15874/comments -u user -p password1`

//Then we add the database to the Herook environment parameters
`heroku config:set MLAB_URI=mongodb://user:password1@ds115874.mlab.com:15874/comments`

//transfer database
`mongodump -h localhost:27017 -d Comments -o ~/workspace/mongodb/dump`

//store to mlab
`mongorestore -h ds115874.mlab.com:15874 -d comments -u user ~/workspace/mongodb/dump/Comments`

//set to use Heroku on production
`heroku config:set NODE_ENV=production`

//set MLAB_URI on heroku so that on production we get data from there
h`eroku config:set MLAB_URI=mongodb://user:password1@ds115874.mlab.com:15874/comments`


//test production mLab on local
`DB_URI=mongodb://user:password1@ds115874.mlab.com:15874/comments nodemon`

//test production mLab on heroku - go to
`https://drugo-ime238.herokuapp.com/api/comments - now api is using mLab database`

//heroku debug
`heroku logs --tail`

